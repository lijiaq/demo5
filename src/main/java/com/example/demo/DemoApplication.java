package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoApplication {

    public static void main(String[] args) {

        int sum=1;
        int i=1;
        int w=0;
        while(sum<=sum*i){
            sum=sum*i;
            w=i;
            i++;}

        System.out.println("int阶乘为"+w+"最大值"+sum);

        long sum1=1;
        long i1=1;
        long w1=0;
        while(sum1<=sum1*i1){
            sum1=sum1*i1;
            w1 =i1;

            i1++;}
        System.out.println("long阶乘为"+w1+"最大值"+sum1);

        short sum2=1;
        short i2=1;
        short w2=0;
        while(sum2<=(short)(sum2*i2)){
            sum2=(short) (sum2*i2);
            w2=i2 ;
            i2++;}
        System.out.println("short阶乘为"+w2+"最大值"+sum2);

        byte sum3=1;
        byte i3=1;
        byte w3=0;
        while(sum3<=(byte)(sum3*i3)){
            sum3=(byte) (sum3*i3);
            w3=i3;
            i3++;}
        System.out.println("byte阶乘为"+w3+"最大值"+sum3);

        int n4=1;
        int sum4=1;
        int w4=0;
        for(char i4=1;i4<n4+1;i4++){
            sum4=sum4*i4;
            if(sum4>0&&sum4<=Character.MAX_VALUE){
                w4=i4 ;
                n4++;
            }
        }System.out.println("char阶乘为"+w4+"最大值"+sum4);
        double n5=1.0;
        double sum5=1.0;
        double w5=0;
        double s = 0;
        for(double i5=1;i5<n5+1;i5++){
            sum5=sum5*i5;
            if(sum5>0&&sum5<=Double.MAX_VALUE){
                s=sum5;
                w5=i5;
                n5++;
            }

        }
        System.out.println("double阶乘为"+w5+"最大值"+s);
        float n6=1.0f;
        float sum6=1.0f;
        float w6=0;
        float s1=0f;
        for(float i6=1;i6<n6+1;i6++){
            sum6=sum6*i6;
            if(sum6>0&&sum6<=Float.MAX_VALUE){
                s1=sum6;
                w6=i6 ;
                n6++;
            }
        }System.out.println("float阶乘为"+w6+"最大值"+s1);
        SpringApplication.run(DemoApplication.class, args);
    }

}
